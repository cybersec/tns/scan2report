#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Standard libraries
from dataclasses import dataclass, field
import base64
import getopt
import glob
import hashlib
import ipaddress
import json
import locale
import math
import os
import pprint
import re
import sys
import urllib.parse
from copy import deepcopy
from os import path
from xml.etree import ElementTree
from typing import List, Dict, Any

# External libraries
import pandas
from tqdm import tqdm
import cloudscraper
from urlextract import URLExtract

DOCX_SUPPORT = os.getenv("DISABLE_DOCX_SUPPORT", "yes").lower() in ['yes', 'y', 'true']  # Disable this if you don't need docx output and want to have easier setup.

if DOCX_SUPPORT:
    import docx
    from docx import Document
    from docx.enum.dml import MSO_THEME_COLOR_INDEX
    from docx.enum.table import WD_ALIGN_VERTICAL
    from docx.oxml.text.paragraph import CT_P
    from docx.oxml.table import CT_Tbl
    from docx.table import Table
    from docx.text.paragraph import Paragraph
    from docx.oxml import parse_xml
    from docx.oxml.ns import nsdecls

    locale.setlocale(locale.LC_ALL, "cs_CZ.UTF-8")


PROOF_SEPARATOR = '\n\n========================================\n'

TRANSLATE = {
    'cs': {
        'list_of_services': 'Seznam identifikovaných otevřených portů a detekovaných služeb',
        'summary_of_findings': 'Shrnutí detekovaných zranitelností',
        'list_of_findings': 'Seznam nálezů',
        'name': 'Název',
        'hostname': 'Název hostitele',
        'ip_address': 'IP adresa',
        'port': 'Číslo portu',
        'port_number': 'Číslo portu a protokol',
        'protocol': 'Protokol',
        'service_name': 'Název služby',
        'severity': 'Závažnost',
        'severity_value': {
            0: 'Informativní',
            1: 'Nízká',
            2: 'Střední',
            3: 'Vysoká',
            4: 'Kritická'
        },
        'attack_complexicity': 'Komplexita útoku',
        'attack_complexicity_value': {
            0: 'Není k dispozici',
            1: 'Nízká',
            2: 'Střední',
            3: 'Vysoká'
        },
        'system_impact': 'Dopad na systém',
        'system_impact_value': {
            0: 'Není k dispozici',
            1: 'Nízký',
            2: 'Střední',
            3: 'Vysoký'
        },
        'hosts': 'Výskyt',
        'description': 'Popis',
        'impact': 'Důsledky',
        'recommendations': 'Doporučení',
        'links': 'Další zdroje'
    },
    'en': {
        'list_of_services': 'List of open ports and running services',
        'summary_of_findings': 'Summary of findings',
        'list_of_findings': 'List of findings',
        'name': 'Name',
        'hostname': 'Hostname',
        'ip_address': 'IP address',
        'port': 'Port number',
        'port_number': 'Port number and protocol',
        'protocol': 'Protocol',
        'service_name': 'Service',
        'severity': 'Risk level',
        'severity_value': {
            0: 'Informative',
            1: 'Low',
            2: 'Medium',
            3: 'High',
            4: 'Critical'
        },
        'attack_complexicity': 'Attack complexity',
        'attack_complexicity_value': {
            0: 'N/A',
            1: 'Low',
            2: 'Medium',
            3: 'High'
        },
        'system_impact': 'Impact',
        'system_impact_value': {
            0: 'N/A',
            1: 'Low',
            2: 'Medium',
            3: 'High'
        },
        'hosts': 'Affected component',
        'description': 'Description',
        'impact': 'Impact',
        'recommendations': 'Recommendations',
        'links': 'Additional resources'
    }
}

COLORS_CONST = ['D9D9D9', '92D050', 'FFFF00', 'FFC000', 'FF0000']

debug: bool = False


@dataclass
class Scan2ReportConfig:
    SUPPORTED_OUTFILE_FORMATS: List[str] = field(default_factory=lambda: ['csv', 'json'] + (['docx'] if DOCX_SUPPORT else []))
    TEMPLATE_DOC_DEFAULT: str = 'templates/tns.docx' if DOCX_SUPPORT else 'DOCX_NOT_AVAILABLE'

    lang: str = 'cs'
    ignore: List[str] = field(default_factory=list)
    ignorePluginOutput: bool = False
    include: List[str] = field(default_factory=list)
    minSeverity: int = 0
    outfile: str = 'out.docx'
    outfileFormat: str = 'docx'
    pluginStatsOnly: bool = False
    profile: str = 'default'
    rootDir: str = os.path.dirname(os.path.realpath(__file__))  # note that changing rootDir AFTER init doesn't change templateDoc. Do it manually, or set rootDir during init.
    resolveRedirects: bool = False
    skipServices: bool = False
    skipTcpWrapped: bool = False
    templateDoc: str = None  # filled using __post_init__
    boldStart: str = '**'
    boldEnd: str = '**'

    def __post_init__(self):
        if self.templateDoc is None:
            self.templateDoc = os.path.join(self.rootDir, self.TEMPLATE_DOC_DEFAULT)
    
    def usage(self):
        print('Usage:', sys.argv[0], '[OPTIONS] .. <FILE> ..\n',
              '   <FILE>: Input files. Supported formats: Nessus XML report, Burp, Lynis Report (.dat), KubeAudit, CHAPS.\n',
              '   \n',
              '   OPTIONS:\n',
              '       -d, --debug (enables also plugin IDs in output document)\n',
              '       -i, --ignore <plugin_ids>: comma separated list of plugin ids to be ignored (ex. nessus_57582), default: none\n',
              '       -I, --include <plugin_ids>: include only specified plugins (ex. nessus_57582), default: none\n',
              '       -l, --lang <language>: report language (cs, en), default:', self.lang, '\n',
              '       -m, --min-severity <num>: minimal severity (0 - info ... 4 - critical), default:', self.minSeverity, '\n',
              '       -o, --output <file>: write output to file, default:', self.outfile, ', supported formats:', self.SUPPORTED_OUTFILE_FORMATS, '\n',
              '       -p, --profile <name>: use selected profile (default, internal), defaut value:', self.profile, '\n',
              '       -r, --resolve-redirects: resolve redirects via nessus.com, default:', self.resolveRedirects, '\n',
              '       -s, --skip-services: do not generate open ports table, default:', self.skipServices, '\n',
              '       -S, --plugin-stats: do not generate report, only plugin hit stats, default:', self.pluginStatsOnly, '\n',
              '       -t, --template <file>: use specified template document, default:', self.templateDoc, '\n',
              '       -W, --skip-tcp-wrapped: ignore open ports detected as TCP wrapped, default:', self.skipTcpWrapped, '\n')

    def parse_arguments(self, argv: List[str]):
        # parse arguments
        try:
            opts, args = getopt.getopt(argv, 'dhi:I:l:m:no:p:sSt:W', ['debug', 'help', 'ignore=', 'include=', 'lang=', 'min-severity=', 'output=', 'profile=', 'resolve-redirects', 'skip-services', 'plugin-stats', 'template', 'skip-tcp-wrapped'])
        except getopt.GetoptError:
            self.usage()
            sys.exit(2)
        for opt, arg in opts:
            if opt in ('-d', '--debug'):
                global debug
                debug = True
                dprint("Warning: Debug is a global variable and does NOT reset automatically.")
            elif opt in ('-h', '--help'):
                self.usage()
                sys.exit()
            elif opt in ('-i', '--ignore'):
                self.ignore = arg.split(',')
            elif opt in ('-I', '--include'):
                self.include = arg.split(',')
            elif opt in ('-l', '--lang'):
                self.lang = arg
            elif opt in ('-m', '--min-severity'):
                self.minSeverity = int(arg)
            elif opt in ('-o', '--output'):
                self.outfile = arg
            elif opt in ('-p', '--profile'):
                self.profile = arg
            elif opt in ('-r', '--resolve-redirects'):
                self.resolveRedirects = False
            elif opt in ('-s', '--skip-services'):
                self.skipServices = True
            elif opt in ('-S', '--plugin-stats'):
                self.pluginStatsOnly = True
            elif opt in ('-t', '--template'):
                self.templateDoc = os.path.realpath(arg)
                if not (os.path.isfile(self.templateDoc)):
                    print('File "{}" doesn\'t exist or isn\'t readable'.format(self.templateDoc))
                    sys.exit()
            elif opt in ('-W', '--skip-tcp-wrapped'):
                self.skipTcpWrapped = True
        if not self.templateDoc:
            self.templateDoc = self.rootDir + '/' + self.TEMPLATE_DOC_DEFAULT

        if self.outfile:
            self.outfileFormat = re.sub(r'.*\.', '', self.outfile.lower())
            if self.outfileFormat not in self.SUPPORTED_OUTFILE_FORMATS:
                print(f'Unknown output file format: {self.outfileFormat}')
                sys.exit()

        return args

    def apply_profile(self):
        rootDir = self.rootDir
        profile = self.profile
        # select profile
        with open(rootDir + '/plugins/profiles.json', encoding="utf8") as jsonfile:
            try:
                profiles = json.load(jsonfile)
            except json.decoder.JSONDecodeError as err:
                print('Error parsing file ' + rootDir + '/plugins/profiles.json:\n' + format(err))
                sys.exit(1)
        if profile not in profiles:
            print('Profile "' + profile + '" not found!')
            sys.exit(1)
        if 'ignore_plugins' in profiles[profile]:
            self.ignore += profiles[profile]['ignore_plugins']
        if 'ignore_pluginoutput' in profiles[profile] and profiles[profile]['ignore_pluginoutput'].lower() == 'true':
            self.ignorePluginOutput = True
        if 'skip_services' in profiles[profile] and profiles[profile]['skip_services'].lower() == 'true':
            self.skipServices = True


class Scan2ReportData:
    def __init__(self, config: Scan2ReportConfig):
        self.config: Scan2ReportConfig = config
        self.fidAliases: Dict[str, List[str]] = get_all_aliases(config.rootDir)
        self.findings: Dict[str, Dict[str, Any]] = {}  # [FID][finding_field] = field_value
        self.missingPlugins: List[str] = []
        self.services: Dict[str, Dict[int, Dict[str, str]]] = {}  # [IP][PORT][TCP/UPD] = service_name
        self.unknownCounter: int = 0
        self.plugin_stats: Dict[str, Dict] = {}
        self.hostnames: Dict[str, str] = {}


# debug messages
def dprint(message):
    global debug
    if debug:
        print('DEBUG: ' + message, file=sys.stderr)
    return 0


# sort list of ips
def ipsort(iplist):
    try:
        return sorted(list(set(iplist)), key=lambda ip: ipaddress.IPv4Address(re.sub(r':.*', '', ip)))
    except:
        return sorted(list(set(iplist)))


# converts low, medium and high to number
def lmh2num(char):
    if char == 'L':
        return 1
    if char == 'M':
        return 2
    if char == 'H':
        return 3
    return 0


def sort_keys_in_templates():
    root_dir = Scan2ReportConfig().rootDir
    template_filepaths = list(glob.glob(f"{root_dir}/plugins/*/*.json", recursive=True))
    for filepath in tqdm(template_filepaths):
        with open(filepath, 'r', encoding='utf8') as f:
            try:
                data = json.load(f)
            except:
                print(f"Failed to load {filepath}")
                print(f.read())
        with open(filepath, 'w', encoding='utf8') as f:
            json.dump(data, f, indent=4, sort_keys=True, ensure_ascii=False)


def write_new_finding(s2r: Scan2ReportData, template: Dict[str, Any], fid: str, lang: str = None):
    if lang is None:
        lang = s2r.config.lang

    s2r.unknownCounter += 1
    s2r.missingPlugins.append(fid)

    if s2r.config.pluginStatsOnly:
        pluginStatsCounter(s2r.plugin_stats, fid, template['name_original'], template['severity_original'], False)
        return

    filepath = s2r.config.rootDir + '/plugins/' + lang + '/templates/' + fid + '.json'
    dprint('Creating new template ' + filepath)
    with open(filepath, 'w', encoding="utf8") as jsonfile:
        json.dump(template, jsonfile, indent=4, sort_keys=True, ensure_ascii=False)


# get finding severity (for sorting)
def pluginsByFindingSeverity(plugin, lang: str, rootDir: str):
    if os.path.isfile(rootDir + '/plugins/' + lang + '/' + plugin['id'] + '.json'):
        with open(rootDir + '/plugins/' + lang + '/' + plugin['id'] + '.json', encoding="utf8") as jsonfile:
            try:
                texts = json.load(jsonfile)
            except json.decoder.JSONDecodeError as err:
                raise Exception('Error parsing file ' + rootDir + '/plugins/' + lang + '/' + plugin['id'] + '.json:\n' + format(err))
        return texts['severity']
    else:
        return 0


# increase plugin stats counter
def pluginStatsCounter(pluginStats, plugin: str, name: str, severity, known: bool):
    if plugin not in pluginStats:
        pluginStats[plugin] = {}
        pluginStats[plugin]['counter'] = 1
        pluginStats[plugin]['name'] = name
        pluginStats[plugin]['severity'] = severity
        pluginStats[plugin]['known'] = known
    elif pluginStats[plugin]['doCount'] == True:
        pluginStats[plugin]['counter'] += 1
    pluginStats[plugin]['doCount'] = False


# start counting plugin stats again when processing next file
def pluginStatsCounterReset(pluginStats):
    for plugin in pluginStats:
        pluginStats[plugin]['doCount'] = True
    return 0


# resolve ip to hostname
def resolve(ip, hostnames, onlyHostname=False):
    ipWithoutPort = re.sub(r':.*', '', ip)
    if ipWithoutPort in hostnames:
        if onlyHostname:
            return hostnames[ipWithoutPort]
        else:
            return ip + " (" + hostnames[ipWithoutPort] + ")"
    else:
        if onlyHostname:
            return ''
        else:
            return ip


# resolve URL redirects
def resolve_redirects(link, scraper=None):
    if not scraper:
        scraper = cloudscraper.create_scraper()
    r = scraper.get(link)
    if 'Location' in r.headers:
        return resolve_redirects(r.headers['Location'], scraper)
    else:
        return r.url


def resolveFidAliases(aliases: dict, fid: str, rootDir: str):
    dprint(f'Resolving fid alias: "{fid}"')
    keys = [key for key, value in aliases.items() if fid in value]
    if len(keys) == 0:
        dprint(f'No alias found.')
        return fid
    elif len(keys) == 1:
        dprint(f'Found alias: "{keys[0]}"')
        return keys[0]
    else:
        print(f'Error: plugin "{fid}" is in multiple aliases ({rootDir}/plugins/aliases.json):\n{keys}')
        sys.exit()


def produce_findings_as_s2r_dicts(s2r: Scan2ReportData) -> List[Dict[str, Any]]:
    findings = s2r.findings
    hostnames = s2r.hostnames

    print('Generating findings JSON')
    answer = []

    with tqdm(total=len(findings)) as pbar:
        for fid in sorted(findings.keys(),
                          key=lambda x: (-(findings[x]['severity']), locale.strxfrm(findings[x]['name'].lower()))):
            if not debug:
                pbar.update(1)
                pbar.set_description(f"Finding {fid:.20s} ({findings[fid]['name']:.20s})")

            current_finding = findings[fid].copy()
            answer.append(current_finding)

            current_finding["fid"] = fid

            current_finding["hosts"] = []
            for host in ipsort(findings[fid]['hosts']):
                host = re.sub(r':0/tcp', '', host)
                host = resolve(host, hostnames)
                current_finding["hosts"].append(host)

            current_finding["links"] = []
            for link in sorted(findings[fid]['links']):
                current_finding["links"].append(link)

    return answer


# todo: switch reinit_global_vars to True?
def main(argv: List[str], reinit_global_vars: bool = False, s2r_c: Scan2ReportConfig = None) -> Scan2ReportData:
    if reinit_global_vars or s2r_c is None:
        s2r_c = Scan2ReportConfig()

    args = s2r_c.parse_arguments(argv)
    s2r_c.apply_profile()
    s2r = Scan2ReportData(s2r_c)

    if not args:
        s2r_c.usage()
        sys.exit()

    # read input files
    for infile in args:
        if not (os.path.isfile(infile) and os.access(infile, os.R_OK)):
            print('File "{}" doesn\'t exist or isn\'t readable'.format(infile))
            continue
        parse_input_file(debug, s2r, infile)

    # plugin stats
    if s2r_c.pluginStatsOnly:
        print_plugin_stats(s2r)
        sys.exit(1)

    # merge plugin_output into description
    if s2r.findings and s2r_c.outfileFormat in ['docx', 'json']:
        # todo: why it shouldn't be also for CSV?
        combine_plugin_output_and_description(s2r)

    group_findings(s2r)
    resolve_http_redirects(debug, s2r)
    missing_as_list = create_missing_templates(s2r)
    if s2r.unknownCounter > 0:
        print('Template for', s2r.unknownCounter, 'new finding(s) written to', s2r.config.rootDir + '/plugins/' + s2r.config.lang + '/templates/')
    output_to_file(s2r, missing_as_list)

    return s2r


def output_to_file(s2r: Scan2ReportData, missing_as_list):
    dprint('Creating output document')
    outfile = s2r.config.outfile
    outfileFormat = s2r.config.outfileFormat

    if outfileFormat == 'docx' and DOCX_SUPPORT:
        DocxWriter.output_docx(debug, s2r)
    elif outfileFormat == 'csv':
        output_csv(debug, s2r)
    elif outfileFormat == 'json':
        findings_dicts = produce_findings_as_s2r_dicts(s2r)
        with open(outfile, 'w', encoding="utf8") as f:
            json.dump(findings_dicts, f, indent=4, sort_keys=True)
        with open(f"{outfile}.meta.json", 'w', encoding="utf8") as f:  # todo: make this a CLI argument?
            json.dump({
                'missing': missing_as_list,
                'services': s2r.services
            }, f, indent=4, sort_keys=True)
    else:
        raise Exception('Unknown output file format:' + outfileFormat)
    print('Report written:', outfile)


def print_plugin_stats(s2r):
    pluginStats = s2r.plugin_stats
    # pp = pprint.PrettyPrinter(indent=4)
    # pp.pprint(pluginStats)
    print(f'"plugin id";"name";"counter";"severity";"template exists"')
    for plugin in sorted(pluginStats, key=lambda x: (pluginStats[x]['counter'], pluginStats[x]['severity']), reverse=True):
        print(f'"{plugin}";"{pluginStats[plugin]["name"]}";{pluginStats[plugin]["counter"]};{pluginStats[plugin]["severity"]};"{pluginStats[plugin]["known"]}"')


def combine_plugin_output_and_description(s2r: Scan2ReportData):
    findings = s2r.findings
    hostnames = s2r.hostnames

    for fid in sorted(findings.keys(), key=lambda x: (-(findings[x]['severity']), locale.strxfrm(findings[x]['name'].lower()))):
        if not findings[fid]['plugin_output']:
            continue

        description_text = PROOF_SEPARATOR
        for key in findings[fid]['plugin_output'].keys():
            description_text += '\n'
            for host in ipsort(ipsort(findings[fid]['plugin_output'][key])):
                host = re.sub(r':0/tcp', '', host)
                host = resolve(host, hostnames)
                description_text += host + "\n"
            description_text += "\n"
            description_text += re.sub(r'^\n*', '', re.sub(r'\n\n*$', '\n', key))
        findings[fid]['description'] += description_text


def create_missing_templates(s2r: Scan2ReportData):
    missingPlugins = s2r.missingPlugins
    rootDir = s2r.config.rootDir
    lang = s2r.config.lang

    missing_as_list = []
    if len(missingPlugins) == 0:
        return missing_as_list

    unique = []
    for item in missingPlugins:
        if item not in unique:
            unique.append(item)
    print('Still missing template for following finding(s), the report has been generated without them:')
    for missing in sorted(unique):
        try:
            with open(rootDir + '/plugins/' + lang + '/templates/' + missing + '.json', encoding="utf8") as jsonfile:
                try:
                    texts = json.load(jsonfile)
                except json.decoder.JSONDecodeError as err:
                    raise Exception('Error parsing file ' + rootDir + '/plugins/' + lang + '/templates/' + missing + '.json:\n' + format(err))
        except FileNotFoundError as err:
            texts = {}

        if 'name' in texts and texts['name'] != '':
            name = texts['name']
        elif 'name_original' in texts and texts['name_original'] != '':
            name = texts['name_original']
        else:
            name = 'unknown'

        if 'severity' in texts and texts['severity'] != '':
            severity = texts['severity']
        elif 'severity_original' in texts and texts['severity_original'] != '':
            severity = texts['severity_original']
        else:
            severity = 'unknown'
        missing_single_txt_output = f"\t{missing}.json\t{severity} ({TRANSLATE[lang]['severity_value'][severity]})\t{name}"
        print(missing_single_txt_output)

        missing_as_list.append(missing_single_txt_output)

    return missing_as_list


def get_all_aliases(rootDir: str):
    # get finding id aliases
    with open(rootDir + '/plugins/aliases.json', encoding="utf8") as jsonfile:
        try:
            fidAliases = json.load(jsonfile)
        except json.decoder.JSONDecodeError as err:
            print('Error parsing file ' + rootDir + '/plugins/aliases.json:\n' + format(err))
            sys.exit(1)
    return fidAliases


def parse_input_file(debug: bool, s2r: Scan2ReportData, infile: str):
    if not s2r.config.pluginStatsOnly:
        print('Parsing file ' + infile)
    infileFormat = 'unknown'
    pluginStatsCounterReset(s2r.plugin_stats)

    # Nessus
    if infileFormat == 'unknown':
        infileFormat, root = detect_nessus(infile)
        if infileFormat == 'nessus':
            return parse_nessus(debug, s2r, root)
        del root

    # Burp
    if infileFormat == 'unknown':
        infileFormat, root = detect_burp(infile)
        if infileFormat == 'burp':
            return parse_burp(debug, s2r, root)
        del root

    # Lynis
    if infileFormat == 'unknown':
        with open(infile, encoding="utf8") as f:
            line = f.readline()
            if line.startswith('# Lynis Report'):
                return parse_lynis(s2r, infile)
            del line

    # kubeaudit
    if infileFormat == 'unknown':
        with open(infile, encoding="utf8") as f:
            _ = f.readline()
            line = f.readline()
            if line.startswith('---------------- Results for ---------------'):
                return parse_kubeaudit(s2r, infile)
            del line

    # TNS Windows audit
    if infileFormat == 'unknown':
        with open(infile, encoding="utf8") as f:
            try:
                data = json.load(f)
                if "tns-audit-windows" in data:
                    infileFormat = 'tns-audit-windows'
            except json.decoder.JSONDecodeError as err:
                pass
        if infileFormat == 'tns-audit-windows':
            return parse_tns_audit_windows(s2r, infile)

    # TNS custom finding
    if infileFormat == 'unknown':
        with open(infile, encoding="utf8") as f:
            try:
                data = json.load(f)
                if "tns-custom-findings" in data:
                    infileFormat = 'tns-custom-findings'
            except json.decoder.JSONDecodeError as err:
                pass
        if infileFormat == 'tns-custom-findings':
            return parse_tns_custom_finding(s2r, infile)

    print('Error, unknown input file format:', infile)
    print(f"In case of JSON file, try parse it by running the following command to check for syntax errors:")
    print(f'   jq . "{infile}"')
    sys.exit(2)


def parse_tns_custom_finding(s2r: Scan2ReportData, infile: str):
    findings = s2r.findings
    ignore = s2r.config.ignore
    include = s2r.config.include
    lang = s2r.config.lang
    rootDir = s2r.config.rootDir
    plugin_stats = s2r.plugin_stats
    pluginStatsOnly = s2r.config.pluginStatsOnly
    missingPlugins = s2r.missingPlugins

    dprint('Parsing file as tns-custom-findings')
    with open(infile, encoding="utf8") as fp:
        data = json.load(fp)["tns-custom-findings"]
    for fid in data:
        if fid in (ignore):
            dprint('ignoring plugin ' + fid)
            continue
        if include and not fid in (include):
            dprint('Ignoring plugin ' + fid)
            continue

        # generate finding
        if os.path.isfile(rootDir + '/plugins/' + lang + '/' + fid + '.json'):
            # we know this plugin
            dprint('using existing plugin ' + rootDir + '/plugins/' + lang + '/' + fid + '.json')
            if not fid in findings:
                dprint('adding ' + fid + ' to the list of findings')
                with open(rootDir + '/plugins/' + lang + '/' + fid + '.json', encoding="utf8") as jsonfile:
                    try:
                        texts = json.load(jsonfile)
                    except json.decoder.JSONDecodeError as err:
                        raise Exception('Error parsing file ' + rootDir + '/plugins/' + lang + '/' + fid + '.json:\n' + format(err))
                pluginStatsCounter(plugin_stats, fid, texts['name'], texts['severity'], True)
                findings[fid] = {}
                findings[fid]['hosts'] = []
                findings[fid]['plugin_output'] = {}
                findings[fid]['name'] = texts['name']
                findings[fid]['severity'] = texts['severity']
                findings[fid]['attack_complexicity'] = texts['attack_complexicity']
                findings[fid]['system_impact'] = texts['system_impact']
                findings[fid]['description'] = texts['description']
                findings[fid]['impact'] = texts['impact']
                findings[fid]['recommendations'] = texts['recommendations']
                findings[fid]['links'] = texts['links']

            for host in data[fid]:
                dprint('adding ' + host + ' to the list of hosts for finding ' + fid)
                if host not in findings[fid]['hosts']:
                    findings[fid]['hosts'].append(host)
        elif pluginStatsOnly or (not os.path.isfile(rootDir + '/plugins/' + lang + '/templates/' + fid + '.json') and not glob.glob(rootDir + '/plugins/' + lang + '/templates/' + fid + '.json*') and not glob.glob(rootDir + '/plugins/' + lang + '/templates/' + fid + '_*.json')):
            # we don't know this plugin yet
            dprint('creating template for unknown plugin ' + fid + ' (' + rootDir + '/plugins/' + lang + '/templates/' + fid + '.json)')
            template = {}
            links = []
            template['name_original'] = ''
            template['name'] = ''
            template['severity_original'] = 0
            template['severity'] = 0
            template['attack_complexicity_original'] = 0
            template['attack_complexicity'] = 0
            template['system_impact_original'] = 0
            template['system_impact'] = 0
            template['description_original'] = ''
            template['description'] = ''
            template['impact_original'] = ''
            template['impact'] = ''
            template['recommendations_original'] = ''
            template['recommendations'] = ''
            template['links_original'] = []
            template['links'] = []
            template['author'] = ''
            write_new_finding(s2r, template, fid, lang)
        else:
            dprint('An unprocessed template already exists: ' + rootDir + '/plugins/' + lang + '/templates/' + fid + '.json')
            missingPlugins.append(fid)


def parse_tns_audit_windows(s2r: Scan2ReportData, infile: str):
    findings = s2r.findings
    ignore = s2r.config.ignore
    include = s2r.config.include
    lang = s2r.config.lang
    rootDir = s2r.config.rootDir
    plugin_stats = s2r.plugin_stats
    pluginStatsOnly = s2r.config.pluginStatsOnly
    missingPlugins = s2r.missingPlugins
    fidAliases = s2r.fidAliases

    dprint('Parsing file as tns-audit-windows')
    with open(infile, encoding="utf8") as fp:
        data = json.load(fp)["tns-audit-windows"]
        hostname = data["hostname"]
        if "chaps" in data:
            chaps = {}
            for line in base64.b64decode(data["chaps"].encode('ascii')).decode('ascii').splitlines():
                line = line.rstrip()
                if line.startswith('[-] '):
                    line = re.sub(r'^{0}'.format(re.escape('[-] ')), '', line)
                    if ':' in line:
                        varname = re.sub(r'(.*?)\s*:\s*(.*)', '\\1', line)
                        varvalue = re.sub(r'(.*?)\s*:\s*(.*)', '\\2', line)
                        if not varname in chaps:
                            chaps[varname] = {}
                        chaps[varname]['found'] = True
                        if varvalue:
                            if not 'plugin_output' in chaps[varname]:
                                chaps[varname]['plugin_output'] = []
                            chaps[varname]['plugin_output'].append(varvalue)
                    else:
                        if not varname in chaps:
                            chaps[varname] = {}
                        chaps[varname]['found'] = True
                    # pluginoutput: list of local admins
                    if "in local administrators group" in varname:
                        with open(infile, encoding="utf8") as fp2:
                            for line2 in fp2:
                                line2 = line2.rstrip()
                                if 'account in local administrator group:' in line2:
                                    varvalue2 = re.sub(r'(.*?)\s*:\s*(.*)', '\\2', line2)
                                    if not 'plugin_output' in chaps[varname]:
                                        chaps[varname]['plugin_output'] = []
                                    chaps[varname]['plugin_output'].append(varvalue2)
    for chapsfinding in chaps.keys():
        hashobj = hashlib.sha1(chapsfinding.encode('utf-8'))
        fid = 'chaps_' + hashobj.hexdigest()
        fid = resolveFidAliases(fidAliases, fid, rootDir)
        if fid in (ignore):
            dprint('ignoring plugin ' + fid)
            continue
        if include and not fid in (include):
            dprint('Ignoring plugin ' + fid)
            continue

        # generate finding
        if os.path.isfile(rootDir + '/plugins/' + lang + '/' + fid + '.json'):
            # we know this plugin
            dprint('using existing plugin ' + rootDir + '/plugins/' + lang + '/' + fid + '.json')
            if not fid in findings:
                dprint('adding ' + fid + ' to the list of findings')
                with open(rootDir + '/plugins/' + lang + '/' + fid + '.json', encoding="utf8") as jsonfile:
                    try:
                        texts = json.load(jsonfile)
                    except json.decoder.JSONDecodeError as err:
                        raise Exception('Error parsing file ' + rootDir + '/plugins/' + lang + '/' + fid + '.json:\n' + format(err))
                pluginStatsCounter(plugin_stats, fid, texts['name'], texts['severity'], True)
                findings[fid] = {}
                findings[fid]['hosts'] = []
                findings[fid]['plugin_output'] = {}
                findings[fid]['name'] = texts['name']
                findings[fid]['severity'] = texts['severity']
                findings[fid]['attack_complexicity'] = texts['attack_complexicity']
                findings[fid]['system_impact'] = texts['system_impact']
                findings[fid]['description'] = texts['description']
                findings[fid]['impact'] = texts['impact']
                findings[fid]['recommendations'] = texts['recommendations']
                findings[fid]['links'] = texts['links']

            dprint('adding ' + hostname + ' to the list of hosts for finding nessus_' + fid)
            if hostname not in findings[fid]['hosts']:
                findings[fid]['hosts'].append(hostname)
            if 'plugin_output' in chaps[chapsfinding]:
                pluginoutput = '\n'.join(chaps[chapsfinding]['plugin_output'])
                if not pluginoutput in findings[fid]['plugin_output']:
                    findings[fid]['plugin_output'][pluginoutput] = []
                findings[fid]['plugin_output'][pluginoutput].append(hostname)
        elif pluginStatsOnly or (not os.path.isfile(rootDir + '/plugins/' + lang + '/templates/' + fid + '.json') and not glob.glob(rootDir + '/plugins/' + lang + '/templates/' + fid + '.json*') and not glob.glob(rootDir + '/plugins/' + lang + '/templates/' + fid + '_*.json')):
            # we don't know this plugin yet
            dprint('creating template for unknown plugin ' + fid + ' (' + rootDir + '/plugins/' + lang + '/templates/' + fid + '.json)')
            template = {}
            template['name_original'] = ''
            template['name'] = ''
            template['severity_original'] = 0
            template['severity'] = 0
            template['attack_complexicity_original'] = 0
            template['attack_complexicity'] = 0
            template['system_impact_original'] = 0
            template['system_impact'] = 0
            template['description_original'] = chapsfinding
            if 'plugin_output' in chaps[chapsfinding]:
                template['description_original'] += ' (plugin napr. pise: ' + chaps[chapsfinding]['plugin_output'][0] + ')'
            template['description'] = ''
            template['impact_original'] = ''
            template['impact'] = ''
            template['recommendations_original'] = ''
            template['recommendations'] = ''
            template['links_original'] = []
            template['links'] = []
            template['author'] = ''
            write_new_finding(s2r, template, fid, lang)
        else:
            dprint('An unprocessed template already exists: ' + rootDir + '/plugins/' + lang + '/templates/' + fid + '.json')
            missingPlugins.append(fid)


def parse_kubeaudit(s2r: Scan2ReportData, infile: str):
    findings = s2r.findings
    lang = s2r.config.lang
    rootDir = s2r.config.rootDir
    plugin_stats = s2r.plugin_stats
    pluginStatsOnly = s2r.config.pluginStatsOnly
    missingPlugins = s2r.missingPlugins
    fidAliases = s2r.fidAliases


    dprint('Parsing file as kubeaudit output')
    # parse report file to dictionary
    kubeaudit = {}
    apiVersion = 'unknown'
    kind = 'unknown'
    name = 'unknown'
    namespace = 'unknown'
    section = 'none'  # keep track of which section the line belongs to (section header, finding, ...)
    with open(infile, encoding="utf8") as fp:
        for line in fp:
            # remove ansi color codes and newlines
            line = re.sub(r'(\x9B|\x1B\[)[0-?]*[ -\/]*[@-~]', '', line).rstrip()

            if re.match(r'^---------------- Results for ---------------', line):
                # reset vars when entering new section
                section = 'header'
                apiVersion = 'unknown'
                kind = 'unknown'
                name = 'unknown'
                namespace = 'unknown'
            elif section == 'header' and re.match(r'^  apiVersion: (.+)', line):
                apiVersion = re.sub(r'^  apiVersion: (.+)', '\\1', line)
            elif section == 'header' and re.match(r'^  kind: (.+)', line):
                kind = re.sub(r'^  kind: (.+)', '\\1', line)
            elif section == 'header' and re.match(r'^    name: (.+)', line):
                name = re.sub(r'^    name: (.+)', '\\1', line)
            elif section == 'header' and re.match(r'^    namespace: (.+)', line):
                namespace = re.sub(r'^    namespace: (.+)', '\\1', line)
            # begining of finding
            elif re.match(r'^-- \[(error|warning)\] (.+)', line):
                # reset vars when entering new section
                section = 'finding'
                container = ''
                description = ''
                fid_name = ''
                pluginOutput = ''
                # ...
                fid_name = re.sub(r'^-- \[(error|warning)\] (.+)', '\\2', line)
                fid = "kubeaudit_" + fid_name
                fid = resolveFidAliases(fidAliases, fid, rootDir)
                severity_text = re.sub(r'^-- \[(error|warning)\] (.+)', '\\1', line)
                if severity_text == 'error':
                    severity = 2
                elif severity_text == 'warning':
                    severity = 1
                else:
                    raise Exception('Unknown severity_text:' + severity_text)
            # finding values
            elif section == 'finding' and re.match(r'^   Message: (.+)', line):
                description = re.sub(r'^   Message: (.+)', '\\1', line)
            elif section == 'finding' and re.match(r'^   Metadata:', line):
                section = 'finding-pluginoutput'
            # finding plugin output
            elif section == 'finding-pluginoutput' and re.match(r'^      Container: (.+)', line):
                container = re.sub(r'^      Container: (.+)', '\\1', line)
            elif section == 'finding-pluginoutput' and re.match(r'^      (.+)', line):
                pluginOutput = pluginOutput + line.lstrip() + '\n'
            # end of section
            elif re.match(r'^$', line):
                if section.startswith('finding'):
                    # the section header format is different for namespaces
                    if apiVersion == "v1":
                        namespace = name
                        component = f"{kind} {name}"
                    else:
                        component = f"{kind} {name}.{namespace}"
                    # generate finding
                    if os.path.isfile(rootDir + '/plugins/' + lang + '/' + fid + '.json'):
                        # we know this plugin
                        dprint('Using existing plugin ' + rootDir + '/plugins/' + lang + '/' + fid + '.json')
                        if not fid in findings:
                            dprint('Adding ' + fid + ' to the list of findings')
                            with open(rootDir + '/plugins/' + lang + '/' + fid + '.json', encoding="utf8") as jsonfile:
                                try:
                                    texts = json.load(jsonfile)
                                except json.decoder.JSONDecodeError as err:
                                    raise Exception('Error parsing file ' + rootDir + '/plugins/' + lang + '/' + fid + '.json:\n' + format(err))
                            pluginStatsCounter(plugin_stats, fid, texts['name'], texts['severity'], True)
                            findings[fid] = {}
                            findings[fid]['hosts'] = []
                            findings[fid]['plugin_output'] = {}
                            findings[fid]['name'] = texts['name']
                            findings[fid]['severity'] = texts['severity']
                            findings[fid]['attack_complexicity'] = texts['attack_complexicity']
                            findings[fid]['system_impact'] = texts['system_impact']
                            findings[fid]['description'] = texts['description']
                            findings[fid]['impact'] = texts['impact']
                            findings[fid]['recommendations'] = texts['recommendations']
                            findings[fid]['links'] = texts['links']

                        dprint('Adding ' + component + ' to the list of hosts for finding nessus_' + fid)
                        if component not in findings[fid]['hosts']:
                            findings[fid]['hosts'].append(component)
                        if pluginOutput and not pluginOutput in findings[fid]['plugin_output']:
                            findings[fid]['plugin_output'][pluginOutput] = []
                            findings[fid]['plugin_output'][pluginOutput].append(component)
                    elif pluginStatsOnly or (not os.path.isfile(rootDir + '/plugins/' + lang + '/templates/' + fid + '.json') and not glob.glob(rootDir + '/plugins/' + lang + '/templates/' + fid + '.json*') and not glob.glob(rootDir + '/plugins/' + lang + '/templates/' + fid + '_*.json')):
                        # we don't know this plugin yet
                        dprint('Creating template for unknown plugin ' + fid + ' (' + rootDir + '/plugins/' + lang + '/templates/' + fid + '.json)')
                        template = {}
                        template['name_original'] = fid_name
                        template['name'] = ''
                        template['severity_original'] = severity
                        template['severity'] = 0
                        template['attack_complexicity_original'] = 0
                        template['attack_complexicity'] = 0
                        template['system_impact_original'] = 0
                        template['system_impact'] = 0
                        template['description_original'] = description
                        template['description'] = ''
                        template['impact_original'] = ''
                        template['impact'] = ''
                        template['recommendations_original'] = ''
                        template['recommendations'] = ''
                        template['links_original'] = []
                        template['links'] = []
                        template['author'] = ''
                        write_new_finding(s2r, template, fid, lang)
                    else:
                        dprint('An unprocessed template already exists: ' + rootDir + '/plugins/' + lang + '/templates/' + fid + '.json')
                        missingPlugins.append(fid)
                if section != 'header':
                    section = 'none'


def parse_lynis(s2r: Scan2ReportData, infile: str):
    findings = s2r.findings
    ignore = s2r.config.ignore
    include = s2r.config.include
    lang = s2r.config.lang
    rootDir = s2r.config.rootDir
    plugin_stats = s2r.plugin_stats
    pluginStatsOnly = s2r.config.pluginStatsOnly
    missingPlugins = s2r.missingPlugins
    fidAliases = s2r.fidAliases

    dprint('Parsing file as Lynis output')
    # parse report file to dictionary
    lynis = {}
    lynisPermRecmd = {}
    lynisSysctlRecmd = {}
    with open(infile, encoding="utf8") as fp:
        for line in fp:
            line = line.rstrip()
            varName = re.sub(r'(.*?)=(.*)', '\\1', line)
            varValue = re.sub(r'(.*?)=(.*)', '\\2', line)
            # array
            if '[]' in varName:
                varName = re.sub(r'\[\]', '', varName)
                if not varName in lynis:
                    lynis[varName] = []
                lynis[varName].append(varValue)
            # single value
            else:
                lynis[varName] = varValue
    # hostname
    hostname = 'unkown (' + infile + ')'
    if 'hostname' in lynis:
        hostname = lynis['hostname']
    if 'domainname' in lynis:
        hostname = hostname + '.' + lynis['domainname']
    if 'network_ipv4_address' in lynis:
        if not lynis['network_ipv4_address'][0].startswith('127.'):
            hostname = hostname + ' (' + lynis['network_ipv4_address'][0] + ')'
    # suggestions and warnings
    if 'suggestion' in lynis or 'warning' in lynis:
        lynisFindings = []
        if 'suggestion' in lynis:
            lynisFindings.extend(lynis['suggestion'])
        if 'warning' in lynis:
            lynisFindings.extend(lynis['warning'])

        for lynisFinding in lynisFindings:
            lynisId = re.sub(r'(.*?)\|(.*)', '\\1', lynisFinding)
            fid = 'lynis_' + lynisId
            fid = resolveFidAliases(fidAliases, fid, rootDir)
            if fid in (ignore):
                dprint('Ignoring plugin ' + fid)
                continue
            if include and not fid in (include):
                dprint('Ignoring plugin ' + fid)
                continue

            # pluginOutput
            pluginOutput = re.sub(r'(.*?)\|(.*)', '\\2', lynisFinding)
            pluginOutput = re.sub(r'[\-\|]*$', '', pluginOutput)
            # pluginOutput - wrong file permissions
            if fid == "lynis_FILE-7524":
                # read recommended values
                if os.path.isfile(rootDir + '/templates/tns.prf'):
                    with open(rootDir + '/templates/tns.prf', encoding="utf8") as fp:
                        for line in fp:
                            line = line.rstrip()
                            if re.match(r'^(permfile|permdir)=', line):
                                # permfile=/boot/grub/grub.cfg:rw-------:root:root:WARN:
                                f = re.sub(r'(permfile|permdir)=(.*?):(.*?):(.*?):(.*?):(.*?):', '\\2', line)
                                perm = re.sub(r'(permfile|permdir)=(.*?):(.*?):(.*?):(.*?):(.*?):', '\\3', line)
                                owner = re.sub(r'(permfile|permdir)=(.*?):(.*?):(.*?):(.*?):(.*?):', '\\4:\\5', line)
                                lynisPermRecmd[f] = perm + ' ' + owner
                # read reported files
                lynisConsole = re.sub(r'(.*)/.*', '\\1/lynis.console', infile)
                if os.path.isfile(lynisConsole):
                    with open(lynisConsole, encoding="utf8") as fp:
                        for line in fp:
                            line = line.rstrip()
                            # remove ANSI color codes
                            ansi_escape = re.compile(r'\x1B(?:[@-Z\\-_]|\[[0-?]*[ -/]*[@-~])')
                            line = ansi_escape.sub('', line)
                            if re.match(r'^(File|Directory): ', line):
                                if not '[ OK ]' in line:
                                    f = re.sub(r'.*(File|Directory):\s*([^\s]*)\s*\[.*', '\\2', line)
                                    if f in lynisPermRecmd:
                                        pluginOutput = pluginOutput + "\nDoporučení pro " + f + ":\t" + lynisPermRecmd[f]
                                    else:
                                        pluginOutput = pluginOutput + "\n" + f + ":\t doporučené nastavení nenalezeno!"
            # pluginOutput - wrong home directory permissions
            if fid == "lynis_HOME-9304":
                # read reported files
                lynisConsole = re.sub(r'(.*)/.*', '\\1/lynis.console', infile)
                if os.path.isfile(lynisConsole):
                    with open(lynisConsole, encoding="utf8") as fp:
                        for line in fp:
                            line = line.rstrip()
                            # remove ANSI color codes
                            ansi_escape = re.compile(r'\x1B(?:[@-Z\\-_]|\[[0-?]*[ -/]*[@-~])')
                            line = ansi_escape.sub('', line)
                            if re.match(r'permissions of home directory .* of user .* are not strict enough', line):
                                pluginOutput = pluginOutput + "\n" + line
            # pluginOutput - wrong home directory owner
            if fid == "lynis_HOME-9306":
                # read reported files
                lynisConsole = re.sub(r'(.*)/.*', '\\1/lynis.console', infile)
                if os.path.isfile(lynisConsole):
                    with open(lynisConsole, encoding="utf8") as fp:
                        for line in fp:
                            line = line.rstrip()
                            # remove ANSI color codes
                            ansi_escape = re.compile(r'\x1B(?:[@-Z\\-_]|\[[0-?]*[ -/]*[@-~])')
                            line = ansi_escape.sub('', line)
                            if re.match(r'the home directory .* of user .* is owned by ', line):
                                pluginOutput = pluginOutput + "\n" + line
            # pluginOutput - compilers found
            if fid == "lynis_HRDN-7222":
                # read reported files
                lynisConsole = re.sub(r'(.*)/.*', '\\1/lynis.console', infile)
                if os.path.isfile(lynisConsole):
                    with open(lynisConsole, encoding="utf8") as fp:
                        for line in fp:
                            line = line.rstrip()
                            # remove ANSI color codes
                            ansi_escape = re.compile(r'\x1B(?:[@-Z\\-_]|\[[0-?]*[ -/]*[@-~])')
                            line = ansi_escape.sub('', line)
                            if re.match(r'Binary: found .* world executable', line):
                                pluginOutput = pluginOutput + "\n" + line
            # pluginOutput - sysctl
            if fid == "lynis_KRNL-6000":
                # read recommended values
                if os.path.isfile(rootDir + '/templates/tns.prf'):
                    with open(rootDir + '/templates/tns.prf', encoding="utf8") as fp:
                        for line in fp:
                            line = line.rstrip()
                            if re.match(r'^config-data=sysctl', line):
                                # config-data=sysctl;kernel.randomize_va_space;2;1;Randomize of memory address locations (ASLR);sysctl -a;url:https;//kernel.org/doc/Documentation/sysctl/kernel.txt;category:security;
                                # 1;Randomize of memory address locations (ASLR);sysctl -a;url:https;//kernel.org/doc/Documentation/sysctl/kernel.txt;category:security;
                                line = re.sub(r'(url:https?);//', '\\1://', line)
                                sysctl = re.sub(r'config-data=sysctl;(.*?);.*;', '\\1', line)
                                recmd = re.sub(r'config-data=sysctl;(.*?);(.*?);.*', '\\2', line)
                                descr = re.sub(r'config-data=sysctl;(.*?);(.*?);(.*);', '\\3;', line)
                                url = re.sub(r'(.*?);(.*?);(.*);', '\\3', descr)
                                descr = re.sub(r'(.*?);(.*?);(.*);', '\\2', descr)
                                if re.match(r'.*;url:.*', url):
                                    url = re.sub(r'.*url:(.*?);.*', '\\1', url)
                                    lynisSysctlRecmd[sysctl] = 'doporučená hodnota: ' + recmd + '\tPopis: ' + descr + ' (' + url + ')'
                                else:
                                    lynisSysctlRecmd[sysctl] = 'doporučená hodnota: ' + recmd + '\tPopis: ' + descr
                # read reported files
                lynisConsole = re.sub(r'(.*)/.*', '\\1/lynis.console', infile)
                if os.path.isfile(lynisConsole):
                    with open(lynisConsole, encoding="utf8") as fp:
                        read = False
                        for line in fp:
                            line = line.rstrip()
                            # remove ANSI color codes
                            ansi_escape = re.compile(r'\x1B(?:[@-Z\\-_]|\[[0-?]*[ -/]*[@-~])')
                            line = ansi_escape.sub('', line)
                            if re.match(r'^- Comparing sysctl key pairs with scan profile', line):
                                read = True
                            if re.match(r'^$', line):
                                read = False
                            if read:
                                if not '[ OK ]' in line:
                                    sysctl = re.sub(r'- (.*?) .*', '\\1', line)
                                    if sysctl in lynisSysctlRecmd:
                                        pluginOutput = pluginOutput + "\n" + sysctl + ": " + lynisSysctlRecmd[sysctl]
                                    else:
                                        pluginOutput = pluginOutput + "\n" + sysctl + ": doporučená hodnota nenalezena!"
            # pluginOutput - incorrect owner/insecure file permissions for cronjob
            if fid == "lynis_SCHD-7704":
                # read reported files
                lynisLog = re.sub(r'(.*)/.*', '\\1/lynis.log', infile)
                if os.path.isfile(lynisLog):
                    with open(lynisLog, encoding="utf8") as fp:
                        for line in fp:
                            line = line.rstrip()
                            if 'incorrect owner found for cronjob file' in line or 'insecure file permissions for cronjob file' in line:
                                pluginOutput = pluginOutput + "\n" + line
            # generate finding
            if os.path.isfile(rootDir + '/plugins/' + lang + '/' + fid + '.json'):
                # we know this plugin
                dprint('Using existing plugin ' + rootDir + '/plugins/' + lang + '/' + fid + '.json')
                if not fid in findings:
                    dprint('Adding ' + fid + ' to the list of findings')
                    with open(rootDir + '/plugins/' + lang + '/' + fid + '.json', encoding="utf8") as jsonfile:
                        try:
                            texts = json.load(jsonfile)
                        except json.decoder.JSONDecodeError as err:
                            raise Exception('Error parsing file ' + rootDir + '/plugins/' + lang + '/' + fid + '.json:\n' + format(err))
                    pluginStatsCounter(plugin_stats, fid, texts['name'], texts['severity'], True)
                    findings[fid] = {}
                    findings[fid]['hosts'] = []
                    findings[fid]['plugin_output'] = {}
                    findings[fid]['name'] = texts['name']
                    findings[fid]['severity'] = texts['severity']
                    findings[fid]['attack_complexicity'] = texts['attack_complexicity']
                    findings[fid]['system_impact'] = texts['system_impact']
                    findings[fid]['description'] = texts['description']
                    findings[fid]['impact'] = texts['impact']
                    findings[fid]['recommendations'] = texts['recommendations']
                    findings[fid]['links'] = texts['links']

                dprint('Adding ' + hostname + ' to the list of hosts for finding nessus_' + fid)
                if hostname not in findings[fid]['hosts']:
                    findings[fid]['hosts'].append(hostname)
                if not pluginOutput in findings[fid]['plugin_output']:
                    findings[fid]['plugin_output'][pluginOutput] = []
                findings[fid]['plugin_output'][pluginOutput].append(hostname)
            elif pluginStatsOnly or (not os.path.isfile(rootDir + '/plugins/' + lang + '/templates/' + fid + '.json') and not glob.glob(rootDir + '/plugins/' + lang + '/templates/' + fid + '.json*') and not glob.glob(rootDir + '/plugins/' + lang + '/templates/' + fid + '_*.json')):
                # we don't know this plugin yet
                dprint('Creating template for unknown plugin ' + fid + ' (' + rootDir + '/plugins/' + lang + '/templates/' + fid + '.json)')
                template = {}
                template['name_original'] = ''
                template['name'] = ''
                template['severity_original'] = 0
                template['severity'] = 0
                template['attack_complexicity_original'] = 0
                template['attack_complexicity'] = 0
                template['system_impact_original'] = 0
                template['system_impact'] = 0
                template['description_original'] = 'https://cisofy.com/lynis/controls/' + lynisId + '/ (plugin napr. pise: ' + pluginOutput + ')'
                template['description'] = ''
                template['impact_original'] = ''
                template['impact'] = ''
                template['recommendations_original'] = ''
                template['recommendations'] = ''
                template['links_original'] = []
                template['links'] = []
                template['author'] = ''
                write_new_finding(s2r, template, fid, lang)
            else:
                dprint('An unprocessed template already exists: ' + rootDir + '/plugins/' + lang + '/templates/' + fid + '.json')
                missingPlugins.append(fid)


def parse_burp(debug: bool, s2r: Scan2ReportData, root):
    findings = s2r.findings
    ignore = s2r.config.ignore
    include = s2r.config.include
    lang = s2r.config.lang
    rootDir = s2r.config.rootDir
    plugin_stats = s2r.plugin_stats
    pluginStatsOnly = s2r.config.pluginStatsOnly
    missingPlugins = s2r.missingPlugins
    fidAliases = s2r.fidAliases
    hostnames = s2r.hostnames
    minSeverity = s2r.config.minSeverity
    ignorePluginOutput = s2r.config.ignorePluginOutput

    dprint('Parsing file as Burp report')
    issueCounter = 0
    for issue in root.iter('issue'):
        issueCounter += 1
    with tqdm(total=issueCounter) as pbar:
        for issue in root.iter('issue'):
            issueName = issue.find('./name').text
            if not debug:
                pbar.update(1)
                pbar.set_description(f"Finding {issueName:15}")
            dprint('Processing finding ' + issueName)
            ip = issue.find('./host').attrib['ip']
            url = issue.find('./host').text + issue.find('./path').text
            urlParsed = urllib.parse.urlparse(url)
            hostname = urlParsed.netloc
            if hostname and not ip in hostnames:
                hostnames[ip] = hostname
            fid = 'burp_' + issue.find('./type').text
            fid = resolveFidAliases(fidAliases, fid, rootDir)
            if fid in (ignore):
                dprint('Ignoring plugin ' + fid)
                continue
            if include and not fid in (include):
                dprint('Ignoring plugin ' + fid)
                continue
            if os.path.isfile(rootDir + '/plugins/' + lang + '/' + fid + '.json'):
                # we know this plugin
                dprint('Using existing plugin ' + rootDir + '/plugins/' + lang + '/' + fid + '.json')
                with open(rootDir + '/plugins/' + lang + '/' + fid + '.json', encoding="utf8") as jsonfile:
                    try:
                        texts = json.load(jsonfile)
                    except json.decoder.JSONDecodeError as err:
                        raise Exception('Error parsing file ' + rootDir + '/plugins/' + lang + '/' + fid + '.json:\n' + format(err))
                pluginStatsCounter(plugin_stats, fid, texts['name'], texts['severity'], True)
                if (texts['severity'] < minSeverity):
                    continue
                if not fid in findings:
                    dprint('Adding ' + fid + ' to the list of findings')
                    findings[fid] = {}
                    findings[fid]['hosts'] = []
                    findings[fid]['plugin_output'] = {}
                    findings[fid]['name'] = texts['name']
                    findings[fid]['severity'] = texts['severity']
                    findings[fid]['attack_complexicity'] = texts['attack_complexicity']
                    findings[fid]['system_impact'] = texts['system_impact']
                    findings[fid]['description'] = texts['description']
                    findings[fid]['impact'] = texts['impact']
                    findings[fid]['recommendations'] = texts['recommendations']
                    findings[fid]['links'] = texts['links']

                dprint('Adding ' + url + ' to the list of hosts for finding ' + fid)
                if url not in findings[fid]['hosts']:
                    findings[fid]['hosts'].append(url)

                try:
                    pluginOutput = issue.find('./issueDetail').text
                except AttributeError:
                    pluginOutput = False
                try:
                    poc = issue.find('.//poc').text
                except AttributeError:
                    poc = False
                if poc:
                    if pluginOutput:
                        pluginOutput += '\n' + poc
                    else:
                        pluginOutput = poc
                if pluginOutput and (not ignorePluginOutput or ('ignore_pluginoutput' not in texts or texts['ignore_pluginoutput'].lower() != 'true')):
                    if not pluginOutput in findings[fid]['plugin_output']:
                        findings[fid]['plugin_output'][pluginOutput] = []
                    findings[fid]['plugin_output'][pluginOutput].append(url)
            elif pluginStatsOnly or (not os.path.isfile(rootDir + '/plugins/' + lang + '/templates/' + fid + '.json') and not glob.glob(rootDir + '/plugins/' + lang + '/templates/' + fid + '.json*') and not glob.glob(rootDir + '/plugins/' + lang + '/templates/' + fid + '_*.json')):
                # we don't know this plugin yet
                severity = issue.find('./severity').text
                if severity == 'Critical':
                    severity = 4
                elif severity == 'High':
                    severity = 3
                elif severity == 'Medium':
                    severity = 2
                elif severity == 'Low':
                    severity = 1
                elif severity == 'Information':
                    severity = 0
                else:
                    raise Exception("Cannot determine severity")
                if (int(severity) < minSeverity):
                    continue
                template = {}
                links = []
                name = issueName
                # attack_complexicity and system impact
                attack_complexicity = 0
                system_impact = 0

                # description
                try:
                    description = issue.find('./issueBackground').text
                except AttributeError:
                    description = ''
                # some Burp extensions defines only issueDetail
                if not description:
                    try:
                        description = issue.find('./issueDetail').text
                    except AttributeError:
                        description = ''

                # recommendations
                try:
                    remediationBackground = issue.find('./remediationBackground').text
                except AttributeError:
                    remediationBackground = ''
                try:
                    remediationDetail = issue.find('./remediationDetail').text
                except AttributeError:
                    remediationDetail = ''
                if remediationBackground:
                    recommendations = remediationBackground
                else:
                    recommendations = ''
                if remediationDetail:
                    if recommendations:
                        recommendations += '\n' + remediationDetail
                    else:
                        recommendations = remediationDetail

                extractor = URLExtract()
                # links - references
                try:
                    links += extractor.find_urls(issue.find('./references').text)
                except AttributeError:
                    references = False
                # links - vulnerability classifications
                try:
                    links += extractor.find_urls(issue.find('./vulnerabilityClassifications').text)
                except AttributeError:
                    vulnerabilityClassifications = False

                links = sorted(list(set(links)))
                template['name_original'] = name
                template['name'] = ''
                template['severity_original'] = int(severity)
                template['severity'] = int(severity)
                template['attack_complexicity_original'] = attack_complexicity
                template['attack_complexicity'] = attack_complexicity
                template['system_impact_original'] = system_impact
                template['system_impact'] = system_impact
                template['description_original'] = description
                template['description'] = ''
                template['impact_original'] = ''
                template['impact'] = ''
                template['recommendations_original'] = recommendations
                template['recommendations'] = ''
                template['links_original'] = links
                template['links'] = []
                template['author'] = ''
                write_new_finding(s2r, template, fid, lang)
            else:
                dprint('An unprocessed template already exists: ' + rootDir + '/plugins/' + lang + '/templates/' + fid + '.json')
                missingPlugins.append(fid)


def parse_nessus(debug: bool, s2r: Scan2ReportData, root):
    findings = s2r.findings
    ignore = s2r.config.ignore
    include = s2r.config.include
    lang = s2r.config.lang
    rootDir = s2r.config.rootDir
    plugin_stats = s2r.plugin_stats
    pluginStatsOnly = s2r.config.pluginStatsOnly
    missingPlugins = s2r.missingPlugins
    fidAliases = s2r.fidAliases
    hostnames = s2r.hostnames
    minSeverity = s2r.config.minSeverity
    ignorePluginOutput = s2r.config.ignorePluginOutput
    skipTcpWrapped = s2r.config.skipTcpWrapped
    services = s2r.services

    dprint('Parsing file as Nessus report')
    hostCounter = 0
    namespaces = {'cm': 'http://www.nessus.org/cm'}
    for reportHost in root.iter('ReportHost'):
        hostCounter += 1
    with tqdm(total=hostCounter) as pbar:
        for reportHost in root.iter('ReportHost'):
            ip = reportHost.find('.//tag[@name="host-ip"]').text
            if not debug:
                pbar.update(1)
                pbar.set_description(f"Host {ip:15}")
            dprint('Processing host ' + ip)
            hostname = ''
            if hostname == '':
                try:
                    hostname = reportHost.find('.//tag[@name="host-fqdn"]').text
                except AttributeError:
                    hostname = ''
            if hostname == '':
                try:
                    hostname = reportHost.find('.//tag[@name="netbios-name"]').text
                except AttributeError:
                    hostname = ''
            if hostname == '':
                try:
                    rdns = reportHost.find('.//tag[@name="host-rdns"]').text
                    if rdns != ip:
                        hostname = rdns
                except AttributeError:
                    hostname = ''
            if hostname and not ip in hostnames:
                hostnames[ip] = hostname
            # get open ports
            if skipTcpWrapped:
                # u Port scanners nemáme informaci o "tcp wrapped"
                pluginFamilies = ['Service detection']
            else:
                pluginFamilies = ['Port scanners', 'Service detection']
            for pluginFamily in pluginFamilies:
                for reportItem in reportHost.findall('.//ReportItem[@pluginFamily="' + pluginFamily + '"]'):
                    port = int(reportItem.get('port'))
                    if port == 0:
                        continue
                    try:
                        pluginOutput = reportItem.find('.//plugin_output').text
                    except AttributeError:
                        pluginOutput = False
                    if pluginOutput and "It might be protected by some sort of TCP wrapper" in pluginOutput:
                        continue
                    protocol = reportItem.get('protocol')
                    if not ip in services:
                        services[ip] = {}
                    if not port in services[ip]:
                        services[ip][port] = {}
                    if not protocol in services[ip][port] or services[ip][port][protocol] == 'unknown':
                        services[ip][port][protocol] = reportItem.get('svc_name')
            # get findings
            for reportItem in reportHost.findall('.//ReportItem'):
                port = reportItem.get('port')
                protocol = reportItem.get('protocol')
                try:
                    compliance = reportItem.find('.//compliance').text
                except AttributeError:
                    compliance = False
                fid = 'unknown'
                if compliance:
                    fid = 'nessus_compliance_' + reportItem.find('.//cm:compliance-check-id', namespaces).text
                else:
                    fid = 'nessus_' + reportItem.get('pluginID')
                fid = resolveFidAliases(fidAliases, fid, rootDir)
                if fid in (ignore):
                    dprint('Ignoring plugin ' + fid)
                    continue
                if include and not fid in (include):
                    dprint('Ignoring plugin ' + fid)
                    continue
                if os.path.isfile(rootDir + '/plugins/' + lang + '/' + fid + '.json'):
                    # we know this plugin
                    dprint('Using existing plugin ' + rootDir + '/plugins/' + lang + '/' + fid + '.json')
                    with open(rootDir + '/plugins/' + lang + '/' + fid + '.json', encoding="utf8") as jsonfile:
                        try:
                            texts = json.load(jsonfile)
                        except json.decoder.JSONDecodeError as err:
                            raise Exception('Error parsing file ' + rootDir + '/plugins/' + lang + '/' + fid + '.json:\n' + format(err))
                    pluginStatsCounter(plugin_stats, fid, texts['name'], texts['severity'], True)
                    if (texts['severity'] < minSeverity):
                        continue
                    if not fid in findings:
                        dprint('Adding ' + fid + ' to the list of findings')
                        findings[fid] = {}
                        findings[fid]['hosts'] = []
                        findings[fid]['plugin_output'] = {}
                        findings[fid]['name'] = texts['name']
                        findings[fid]['severity'] = texts['severity']
                        findings[fid]['attack_complexicity'] = texts['attack_complexicity']
                        findings[fid]['system_impact'] = texts['system_impact']
                        findings[fid]['description'] = texts['description']
                        findings[fid]['impact'] = texts['impact']
                        findings[fid]['recommendations'] = texts['recommendations']
                        findings[fid]['links'] = texts['links']

                    dprint('Adding ' + ip + ' to the list of hosts for finding ' + fid)
                    if ip + ':' + port + '/' + protocol not in findings[fid]['hosts']:
                        findings[fid]['hosts'].append(ip + ':' + port + '/' + protocol)
                    try:
                        pluginOutput = reportItem.find('.//plugin_output').text
                    except AttributeError:
                        pluginOutput = False
                    if pluginOutput and (not ignorePluginOutput or ('ignore_pluginoutput' not in texts or texts['ignore_pluginoutput'].lower() != 'true')):
                        if not pluginOutput in findings[fid]['plugin_output']:
                            findings[fid]['plugin_output'][pluginOutput] = []
                        findings[fid]['plugin_output'][pluginOutput].append(ip + ':' + port + '/' + protocol)
                    try:
                        complianceActualValue = reportItem.find('.//cm:compliance-actual-value', namespaces).text
                    except AttributeError:
                        complianceActualValue = False
                    if complianceActualValue and (not ignorePluginOutput or ('ignore_pluginoutput' not in texts or texts['ignore_pluginoutput'].lower() != 'true')):
                        if not complianceActualValue in findings[fid]['plugin_output']:
                            findings[fid]['plugin_output'][complianceActualValue] = []
                        findings[fid]['plugin_output'][complianceActualValue].append(ip)
                elif pluginStatsOnly or (not os.path.isfile(rootDir + '/plugins/' + lang + '/templates/' + fid + '.json') and not glob.glob(rootDir + '/plugins/' + lang + '/templates/' + fid + '.json*') and not glob.glob(rootDir + '/plugins/' + lang + '/templates/' + fid + '_*.json')):
                    # we don't know this plugin yet
                    if compliance:
                        complianceResult = reportItem.find('.//cm:compliance-result', namespaces).text
                        if complianceResult == 'FAILED':
                            severity = 2
                        elif complianceResult == 'WARNING':
                            severity = 0
                        else:
                            continue
                        dprint('Creating template for unknown plugin ' + fid + ' (' + rootDir + '/plugins/' + lang + '/templates/' + fid + '.json)')
                        template = {}
                        links = []
                        name = reportItem.find('.//cm:compliance-check-name', namespaces).text
                        attack_complexicity = 0
                        system_impact = 0
                        # description
                        try:
                            description = reportItem.find('.//description').text
                        except AttributeError:
                            description = ''
                        # recommendations
                        try:
                            recommendations = reportItem.find('.//cm:compliance-solution', namespaces).text
                        except AttributeError:
                            recommendations = ''
                        # links - cve
                        try:
                            seeAlso = reportItem.find('.//cm:compliance-solution', namespaces)
                        except AttributeError:
                            seeAlso = False
                        if (seeAlso):
                            links.extend(seeAlso.split('\n'))
                    else:
                        severity = reportItem.get('severity')
                        if (int(severity) < minSeverity):
                            continue
                        dprint('Creating template for unknown plugin ' + fid + ' (' + rootDir + '/plugins/' + lang + '/templates/' + fid + '.json)')
                        template = {}
                        links = []
                        name = reportItem.get('pluginName')
                        # attack_complexicity and system impact
                        try:
                            cvss3 = reportItem.find('.//cvss3_vector').text
                        except AttributeError:
                            cvss3 = False
                        if (cvss3):
                            cvss_ac = lmh2num(re.sub(r'.*/AC:(.).*', '\\1', cvss3))
                            cvss_c = lmh2num(re.sub(r'.*/C:(.).*', '\\1', cvss3))
                            cvss_i = lmh2num(re.sub(r'.*/I:(.).*', '\\1', cvss3))
                            cvss_a = lmh2num(re.sub(r'.*/A:(.).*', '\\1', cvss3))
                            attack_complexicity = cvss_ac
                            system_impact = int((cvss_c + cvss_i + cvss_a) / 3)
                        else:
                            attack_complexicity = 0
                            system_impact = 0
                        # description
                        try:
                            description = reportItem.find('.//description').text
                        except AttributeError:
                            description = ''
                        # recommendations
                        try:
                            recommendations = reportItem.find('.//solution').text
                        except AttributeError:
                            recommendations = ''
                        # links - cve
                        try:
                            cve = reportItem.find('.//cve').text
                        except AttributeError:
                            cve = False
                        if (cve):
                            links.append('https://nvd.nist.gov/vuln/detail/' + cve)
                        # links - see also
                        try:
                            seeAlso = reportItem.find('.//see_also').text
                        except AttributeError:
                            seeAlso = False
                        if (seeAlso):
                            links.extend(seeAlso.split('\n'))

                    links = sorted(list(set(links)))
                    template['name_original'] = name
                    template['name'] = ''
                    template['severity_original'] = int(severity)
                    template['severity'] = int(severity)
                    template['attack_complexicity_original'] = attack_complexicity
                    template['attack_complexicity'] = attack_complexicity
                    template['system_impact_original'] = system_impact
                    template['system_impact'] = system_impact
                    template['description_original'] = description
                    template['description'] = ''
                    template['impact_original'] = ''
                    template['impact'] = ''
                    template['recommendations_original'] = recommendations
                    template['recommendations'] = ''
                    template['links_original'] = links
                    template['links'] = []
                    template['author'] = ''
                    write_new_finding(s2r, template, fid, lang)
                else:
                    dprint('An unprocessed template already exists: ' + rootDir + '/plugins/' + lang + '/templates/' + fid + '.json')
                    missingPlugins.append(fid)


def detect_burp(infile):
    try:
        tree = ElementTree.parse(infile)
        root = tree.getroot()
        if root.tag == 'issues':
            if root.attrib['burpVersion']:
                return 'burp', root
    except ElementTree.ParseError:
        return 'unknown', None
    except KeyError:
        return 'unknown', None


def detect_nessus(infile):
    try:
        tree = ElementTree.parse(infile)
        root = tree.getroot()
        if root.tag == 'NessusClientData_v2':
            return 'nessus', root
    except ElementTree.ParseError:
        pass
    return 'unknown', None


def group_findings(s2r: Scan2ReportData):
    boldStart = s2r.config.boldStart
    boldEnd = s2r.config.boldEnd
    findings = s2r.findings
    lang = s2r.config.lang
    rootDir = s2r.config.rootDir
    hostnames = s2r.hostnames

    # group findings
    with open(rootDir + '/plugins/groups.json', encoding="utf8") as jsonfile:
        try:
            findingGroups = json.load(jsonfile)
        except json.decoder.JSONDecodeError as err:
            raise Exception('Error parsing file ' + rootDir + '/plugins/groups.json:\n' + format(err))
    for group in findingGroups:
        groupCounter = 0
        for plugin in group['plugins']:
            if plugin['id'] in findings.keys():
                groupCounter += 1
        if groupCounter > 1:
            # for plugin in sorted(group['plugins'], key=lambda x: findings[x['id']]['severity'], reverse=True):
            # for plugin in sorted(group['plugins'], key=lambda x: pluginsByFindingSeverity(findings[x['id']], s2r.config.lang, s2r.config.rootDir), reverse=True):
            for fid in sorted(findings.keys(), key=lambda x: (locale.strxfrm(findings[x]['name'].lower()))):
                for plugin in group['plugins']:
                    if fid == plugin['id']:
                        gid = group.get("gid", group['name']['en'])
                        if not gid in findings:
                            findings[gid] = {}
                            findings[gid]['hosts'] = []
                            findings[gid]['name'] = group['name'][lang]
                            findings[gid]['severity'] = 0
                            findings[gid]['attack_complexicity'] = 0
                            findings[gid]['system_impact'] = 0
                            findings[gid]['description'] = ''
                            findings[gid]['plugin_output'] = {}
                            findings[gid]['impact'] = ''
                            findings[gid]['recommendations'] = ''
                            findings[gid]['links'] = []

                        # hosts
                        for host in findings[fid]['hosts']:
                            if host not in findings[gid]['hosts']:
                                findings[gid]['hosts'].append(host)

                        # severity
                        findings[gid]['severity'] = max(findings[gid]['severity'], findings[fid]['severity'])

                        # attack complexicity
                        if findings[gid]['attack_complexicity'] > 0:
                            findings[gid]['attack_complexicity'] = min(findings[gid]['attack_complexicity'], findings[fid]['attack_complexicity'])
                        if findings[gid]['attack_complexicity'] == 0:
                            findings[gid]['attack_complexicity'] = findings[fid]['attack_complexicity']

                        # system impact
                        findings[gid]['system_impact'] = max(findings[gid]['system_impact'], findings[fid]['system_impact'])

                        # description
                        if findings[gid]['description'] != '':
                            findings[gid]['description'] += '\n\n'
                        findings[gid]['description'] += boldStart + findings[fid]['name'] + ' (' + (TRANSLATE[lang]['severity_value'][findings[fid]['severity']]).lower() + ' ' + (TRANSLATE[lang]['severity']).lower() + ')' + boldEnd + '\n'
                        c = 1
                        for host in findings[fid]['hosts']:
                            host = re.sub(r':0/tcp', '', host)
                            host = resolve(host, hostnames)
                            findings[gid]['description'] += host
                            if c != len(findings[fid]['hosts']):
                                findings[gid]['description'] += '\n'
                                c += 1
                        findings[gid]['description'] += '\n\n' + findings[fid]['description']

                        # description - plugin output
                        for pluginOutput in findings[fid]['plugin_output'].keys():
                            if pluginOutput in findings[gid]['plugin_output'].keys():
                                findings[gid]['plugin_output'][boldStart + findings[fid]['name'] + boldEnd + '\n' + pluginOutput] += []
                                for host in findings[fid]['plugin_output'][pluginOutput]:
                                    if host not in findings[gid]['plugin_output'][boldStart + findings[fid]['name'] + boldEnd + '\n' + pluginOutput]:
                                        findings[gid]['plugin_output'][boldStart + findings[fid]['name'] + boldEnd + '\n' + pluginOutput].append(host)
                            else:
                                findings[gid]['plugin_output'][boldStart + findings[fid]['name'] + boldEnd + '\n' + pluginOutput] = findings[fid]['plugin_output'][pluginOutput]

                        # impact
                        if findings[gid]['impact'] != '':
                            findings[gid]['impact'] += '\n\n'
                        findings[gid]['impact'] += boldStart + findings[fid]['name'] + ' (' + (TRANSLATE[lang]['severity_value'][findings[fid]['severity']]).lower() + ' ' + (TRANSLATE[lang]['severity']).lower() + ')' + boldEnd + '\n' + findings[fid]['impact']

                        # recommendations
                        if findings[gid]['recommendations'] != '':
                            findings[gid]['recommendations'] += '\n\n'
                        findings[gid]['recommendations'] += boldStart + findings[fid]['name'] + ' (' + (TRANSLATE[lang]['severity_value'][findings[fid]['severity']]).lower() + ' ' + (TRANSLATE[lang]['severity']).lower() + ')' + boldEnd + '\n' + findings[fid]['recommendations']

                        # links
                        for link in findings[fid]['links']:
                            if link not in findings[gid]['links']:
                                findings[gid]['links'].append(link)
                        del findings[fid]


def resolve_http_redirects(debug: bool, s2r: Scan2ReportData):
    findings = s2r.findings
    resolveRedirects = s2r.config.resolveRedirects

    if not findings or not resolveRedirects:
        return

    # resolve redirects via http://www.nessus.org/u?e2452f2e
    print(f"Resolving links' redirects")
    with tqdm(total=len(findings)) as pbar:
        for fid in findings.keys():
            if not debug:
                pbar.update(1)
                pbar.set_description(f"Finding {fid:.20s} ({findings[fid]['name']:.20s})")
            for i in range(len(findings[fid]['links'])):
                if re.match(r'.*nessus\.org/u\?.*', findings[fid]['links'][i]):
                    newlink = resolve_redirects(findings[fid]['links'][i])
                    if newlink:
                        dprint(f"Resolved {findings[fid]['links'][i]} to {newlink}")
                        findings[fid]['links'][i] = newlink


def output_csv(debug: bool, s2r: Scan2ReportData):
    findings = s2r.findings
    lang = s2r.config.lang
    outfile = s2r.config.outfile
    services = s2r.services
    skipServices = s2r.config.skipServices
    hostnames = s2r.hostnames

    # print open ports
    if services and not skipServices:
        print('Generating open ports table')
        # ...
    ## print findings summary
    # if findings:
    #    print ('Generating findings summary')
    #    findingsSummary = {}
    #    with tqdm(total=len(findings)) as pbar:
    #        for fid in sorted(findings.keys(), key=lambda x: (-(findings[x]['severity']), locale.strxfrm(findings[x]['name'].lower()))):
    #            if not debug:
    #                pbar.update(1)
    #                pbar.set_description(f"Finding {fid:.20s} ({findings[fid]['name']:.20s})")
    #            dprint('Processing finding '+fid+' - '+findings[fid]['name'])
    #            findingsSummary[fid] = {}
    #            if debug:
    #                findingsSummary[fid][TRANSLATION[lang]['name']] = '[' + fid + '] ' + findings[fid]['name']
    #            else:
    #                findingsSummary[fid][TRANSLATION[lang]['name']] = findings[fid]['name']
    #            findingsSummary[fid][TRANSLATION[lang]['severity']] = TRANSLATION[lang]['severity_value'][findings[fid]['severity']]
    #    df = pandas.DataFrame(data=findingsSummary).T
    #    df.to_csv(outfile, index=False)
    # print findings
    if not findings:
        return

    print('Generating findings')
    findingsList = {}
    with tqdm(total=len(findings)) as pbar:
        for fid in sorted(findings.keys(), key=lambda x: (-(findings[x]['severity']), locale.strxfrm(findings[x]['name'].lower()))):
            if not debug:
                pbar.update(1)
                pbar.set_description(f"Finding {fid:.20s} ({findings[fid]['name']:.20s})")
            dprint('Processing finding ' + fid + ' - ' + findings[fid]['name'])
            for host in ipsort(findings[fid]['hosts']):
                host = re.sub(r':0/tcp', '', host)
                lfid = fid + '@' + host

                # name
                findingsList[lfid] = {}
                if debug:
                    findingsList[lfid][TRANSLATE[lang]['name']] = '[' + fid + '] ' + findings[fid]['name']
                else:
                    findingsList[lfid][TRANSLATE[lang]['name']] = findings[fid]['name']

                # severity
                findingsList[lfid][TRANSLATE[lang]['severity']] = TRANSLATE[lang]['severity_value'][findings[fid]['severity']]

                # attack complexicity
                findingsList[lfid][TRANSLATE[lang]['attack_complexicity']] = TRANSLATE[lang]['attack_complexicity_value'][findings[fid]['attack_complexicity']]

                # system impact
                findingsList[lfid][TRANSLATE[lang]['system_impact']] = TRANSLATE[lang]['system_impact_value'][findings[fid]['system_impact']]

                # hosts (affected components)
                findingsList[lfid][TRANSLATE[lang]['ip_address']] = re.sub(r':.*', '', host)

                # hostname
                findingsList[lfid][TRANSLATE[lang]['hostname']] = resolve(host, hostnames, True)

                # port
                findingsList[lfid][TRANSLATE[lang]['port']] = re.sub(r'([^:]*):?([^/]*)/?(.*)?', '\\2', host)

                # protocol
                findingsList[lfid][TRANSLATE[lang]['protocol']] = re.sub(r'([^:]*):?([^/]*)/?(.*)?', '\\3', host)

                ## description
                # description_text = findings[fid]['description']
                ## toto se tam nevejde
                ##if findings[fid]['plugin_output']:
                ##    for key in findings[fid]['plugin_output'].keys():
                ##        description_text += PROOF_SEPARATOR
                ##        for host in ipsort(ipsort(findings[fid]['plugin_output'][key])):
                ##            host = re.sub(r':0/tcp', '', host)
                ##            host = resolve(host)
                ##            description_text += host + "\n"
                ##        description_text += "\n"
                ##        description_text += re.sub(r'^\n*', '', re.sub(r'\n\n*$', '\n', key))
                # findingsList[lfid][TRANSLATION[lang]['description']] = description_text
                #
                ## impact
                # findingsList[lfid][TRANSLATION[lang]['impact']] = findings[fid]['impact']

                ## recommendations
                # findingsList[lfid][TRANSLATION[lang]['recommendations']] = findings[fid]['recommendations']

                ## links
                # findingsList[lfid][TRANSLATION[lang]['links']] = "\n".join(sorted(findings[fid]['links']))

        df = pandas.DataFrame(data=findingsList).T
        df.to_csv(outfile, index=False)


class DocxWriter:

    @classmethod
    def output_docx(cls, debug: bool, s2r: Scan2ReportData):
        findings = s2r.findings
        hostnames = s2r.hostnames
        lang = s2r.config.lang
        outfile = s2r.config.outfile
        services = s2r.services
        skipServices = s2r.config.skipServices
        templateDoc = s2r.config.templateDoc

        # create DOC from template
        textdoc = Document(templateDoc)
        templateTableServices = textdoc.tables[0]
        templateTblServices = templateTableServices._tbl
        templateTableFindings = textdoc.tables[1]
        templateTblFindings = templateTableFindings._tbl
        templateTableFindingsSummary = textdoc.tables[2]
        templateTblFindingsSummary = templateTableFindingsSummary._tbl
        textdoc._body.clear_content()
        # print open ports
        if services and not skipServices:
            print('Generating open ports table')
            h = textdoc.add_heading(TRANSLATE[lang]['list_of_services'], level=2)
            tblCopy = deepcopy(templateTblServices)
            table = Table(tblCopy, textdoc)
            rowIndex = 1
            with tqdm(total=len(services)) as pbar:
                for ip in ipsort(services.keys()):
                    if not debug:
                        pbar.update(1)
                        pbar.set_description(f"Host {ip:15}")
                    ip_first_row = rowIndex

                    for port in sorted(services[ip].keys()):
                        for protocol in sorted(services[ip][port].keys()):
                            # add row if neccessary
                            if rowIndex > 1:
                                row = table.add_row()
                            else:
                                row = table.rows[rowIndex]
                            # ip adress (only on first row of merged rows)
                            if rowIndex == ip_first_row:
                                host = ip
                                if ip in hostnames:
                                    host += '\n' + hostnames[ip]
                                table.cell(rowIndex, 0).paragraphs[0].text = host
                            table.cell(rowIndex, 1).paragraphs[0].text = str(port) + '/' + protocol
                            table.cell(rowIndex, 2).paragraphs[0].text = services[ip][port][protocol]
                            rowIndex += 1
                    if rowIndex > ip_first_row:
                        first = table.cell(ip_first_row, 0)
                        last = table.cell(rowIndex - 1, 0)
                        merged = last.merge(first)
                        merged.vertical_alignment = WD_ALIGN_VERTICAL.CENTER
                h._p.addnext(table._tbl)
                table.cell(0, 0).paragraphs[0].text = TRANSLATE[lang]['ip_address']
                table.cell(0, 1).paragraphs[0].text = TRANSLATE[lang]['port_number']
                table.cell(0, 2).paragraphs[0].text = TRANSLATE[lang]['service_name']
        # print findings summary
        if findings:
            print('Generating findings summary')
            h = textdoc.add_heading(TRANSLATE[lang]['summary_of_findings'], level=2)
            tblCopy = deepcopy(templateTblFindingsSummary)
            table = Table(tblCopy, textdoc)

            table.cell(0, 0).paragraphs[0].text = ''
            table.cell(0, 0).paragraphs[0].add_run(TRANSLATE[lang]['description']).bold = True
            table.cell(0, 1).paragraphs[0].text = ''
            table.cell(0, 1).paragraphs[0].add_run(TRANSLATE[lang]['severity']).bold = True

            rowIndex = 1
            with tqdm(total=len(findings)) as pbar:
                # for fid in sorted(findings.keys(), key=lambda x: (-(findings[x]['severity']), -(findings[x]['system_impact']), locale.strxfrm(findings[x]['name'].lower()))):
                for fid in sorted(findings.keys(), key=lambda x: (-(findings[x]['severity']), locale.strxfrm(findings[x]['name'].lower()))):
                    if not debug:
                        pbar.update(1)
                        pbar.set_description(f"Finding {fid:.20s} ({findings[fid]['name']:.20s})")
                    dprint('Processing finding ' + fid + ' - ' + findings[fid]['name'])
                    # add row if neccessary
                    if rowIndex > 1:
                        row = table.add_row()
                    else:
                        row = table.rows[rowIndex]
                    cls.setRowColor(table.rows[rowIndex], COLORS_CONST[findings[fid]['severity']])
                    if debug:
                        table.cell(rowIndex, 0).paragraphs[0].text = '[' + fid + '] ' + findings[fid]['name']
                    else:
                        table.cell(rowIndex, 0).paragraphs[0].text = findings[fid]['name']
                    table.cell(rowIndex, 1).paragraphs[0].text = TRANSLATE[lang]['severity_value'][findings[fid]['severity']]
                    rowIndex += 1
                h._p.addnext(table._tbl)
        # print findings
        if findings:
            print('Generating findings')
            h = textdoc.add_heading(TRANSLATE[lang]['list_of_findings'], level=2)
            with tqdm(total=len(findings)) as pbar:
                for fid in sorted(findings.keys(), key=lambda x: (-(findings[x]['severity']), locale.strxfrm(findings[x]['name'].lower()))):
                    if not debug:
                        pbar.update(1)
                        pbar.set_description(f"Finding {fid:.20s} ({findings[fid]['name']:.20s})")
                    dprint('Processing finding ' + fid + ' - ' + findings[fid]['name'])
                    # heading
                    if debug:
                        h = textdoc.add_heading('[' + fid + '] ' + findings[fid]['name'], level=3)
                    else:
                        h = textdoc.add_heading(findings[fid]['name'], level=3)

                    # table
                    tblCopy = deepcopy(templateTblFindings)
                    table = Table(tblCopy, textdoc)

                    # severity - color
                    cls.setRowColor(table.rows[0], COLORS_CONST[findings[fid]['severity']])

                    # severity
                    dprint('Writing severity')
                    table.cell(0, 0).paragraphs[0].text = ''
                    table.cell(0, 0).paragraphs[0].add_run(TRANSLATE[lang]['severity'] + ':').bold = True
                    table.cell(0, 1).paragraphs[0].text = ''
                    table.cell(0, 1).paragraphs[0].add_run(TRANSLATE[lang]['severity_value'][findings[fid]['severity']]).bold = True

                    # attack complexicity and system impact
                    dprint('Writing complexicity and system impact')
                    table.cell(1, 0).paragraphs[0].text = ''
                    table.cell(1, 0).paragraphs[0].add_run(TRANSLATE[lang]['attack_complexicity'] + ':').bold = True
                    table.cell(1, 0).paragraphs[0].add_run(' ' + TRANSLATE[lang]['attack_complexicity_value'][findings[fid]['attack_complexicity']])
                    table.cell(1, 2).paragraphs[0].text = ''
                    table.cell(1, 2).paragraphs[0].add_run(TRANSLATE[lang]['system_impact'] + ':').bold = True
                    table.cell(1, 2).paragraphs[0].add_run(' ' + TRANSLATE[lang]['system_impact_value'][findings[fid]['system_impact']])

                    # hosts (affected components)
                    dprint('Writing affected components')
                    table.cell(2, 0).paragraphs[0].text = ''
                    table.cell(2, 0).paragraphs[0].add_run(TRANSLATE[lang]['hosts'] + ':').bold = True
                    table.cell(2, 1).paragraphs[0].text = ''
                    c = 1
                    for host in ipsort(findings[fid]['hosts']):
                        host = re.sub(r':0/tcp', '', host)
                        host = resolve(host, hostnames)
                        table.cell(2, 1).paragraphs[0].add_run(host)
                        if c != len(findings[fid]['hosts']):
                            table.cell(2, 1).paragraphs[0].add_run('\n')
                            c += 1

                    # description
                    dprint('Writing description')
                    table.cell(3, 0).paragraphs[0].text = ''
                    table.cell(3, 0).paragraphs[0].add_run(TRANSLATE[lang]['description'] + ':').bold = True
                    description_text = findings[fid]['description']
                    table.cell(3, 1).paragraphs[0].text = ''
                    cls.add_md(textdoc, table.cell(3, 1), description_text)

                    # impact
                    dprint('Writing impact')
                    table.cell(4, 0).paragraphs[0].text = ''
                    table.cell(4, 0).paragraphs[0].add_run(TRANSLATE[lang]['impact'] + ':').bold = True
                    table.cell(4, 1).paragraphs[0].text = ''
                    cls.add_md(textdoc, table.cell(4, 1), findings[fid]['impact'])

                    # recommendations
                    dprint('Writing recommendations')
                    table.cell(5, 0).paragraphs[0].text = ''
                    table.cell(5, 0).paragraphs[0].add_run(TRANSLATE[lang]['recommendations'] + ':').bold = True
                    table.cell(5, 1).paragraphs[0].text = ''
                    cls.add_md(textdoc, table.cell(5, 1), findings[fid]['recommendations'])

                    # links
                    dprint('Writing links')
                    table.cell(6, 0).paragraphs[0].text = ''
                    table.cell(6, 0).paragraphs[0].add_run(TRANSLATE[lang]['links'] + ':').bold = True
                    table.cell(6, 1).paragraphs[0].text = ''
                    i = 1
                    for link in sorted(findings[fid]['links']):
                        cls.add_hyperlink(table.cell(6, 1).paragraphs[0], link, (link[:100] + '..') if len(link) > 100 else link, '0000ff', True)
                        if i != len(findings[fid]['links']):
                            table.cell(6, 1).paragraphs[0].add_run('\n')
                        i += 1

                    # end of table
                    h._p.addnext(table._tbl)
        # save
        textdoc.save(outfile)

    @staticmethod
    # add Markdown formatted text
    def add_md(document, parent, text):
        i = 0
        while i < len(text.split('\n')):
            if i != 0:
                templateP = parent.paragraphs[0]._p
                pCopy = deepcopy(templateP)
                p = Paragraph(pCopy, document)
                parent.paragraphs[i - 1]._p.addnext(p._p)
            parent.paragraphs[i].text = ''

            part = text.split('\n')[i]
            chars = []
            attr = {'b': 0, 'i': 0}

            # bold
            j = 0
            while j < len(part):
                if j + 1 < len(part) and part[j] == '*' and part[j + 1] == '*':
                    if attr['b']:
                        attr['b'] = 0
                    else:
                        attr['b'] = 1
                    j += 2
                elif j + 1 < len(part) and part[j] == '_' and part[j + 1] == '_':
                    if attr['i']:
                        attr['i'] = 0
                    else:
                        attr['i'] = 1
                    j += 2
                else:
                    chars.append({'c': part[j], 'attr': {'b': attr['b'], 'i': attr['i']}})
                    j += 1

            for char in chars:
                runner = parent.paragraphs[i].add_run(char['c'])
                if char['attr']['b']:
                    runner.bold = True
                if char['attr']['i']:
                    runner.italic = True

            i += 1
        return

    @staticmethod
    # add hyperlink
    def add_hyperlink(paragraph, url, text, color, underline):
        """
        From https://github.com/python-openxml/python-docx/issues/74#issuecomment-261169410
        A function that places a hyperlink within a paragraph object.

        :param paragraph: The paragraph we are adding the hyperlink to.
        :param url: A string containing the required url
        :param text: The text displayed for the url
        :return: The hyperlink object
        """

        # This gets access to the document.xml.rels file and gets a new relation id value
        part = paragraph.part
        r_id = part.relate_to(url, docx.opc.constants.RELATIONSHIP_TYPE.HYPERLINK, is_external=True)

        # Create the w:hyperlink tag and add needed values
        hyperlink = docx.oxml.shared.OxmlElement('w:hyperlink')
        hyperlink.set(docx.oxml.shared.qn('r:id'), r_id, )

        # Create a w:r element
        new_run = docx.oxml.shared.OxmlElement('w:r')

        # Create a new w:rPr element
        rPr = docx.oxml.shared.OxmlElement('w:rPr')

        # Add color if it is given
        if not color is None:
            c = docx.oxml.shared.OxmlElement('w:color')
            c.set(docx.oxml.shared.qn('w:val'), color)
            rPr.append(c)

        # Underline if it is requested
        u = docx.oxml.shared.OxmlElement('w:u')
        if underline:
            u.set(docx.oxml.shared.qn('w:val'), 'single')
        else:
            u.set(docx.oxml.shared.qn('w:val'), 'none')
        rPr.append(u)

        # Join all the xml elements together add add the required text to the w:r element
        new_run.append(rPr)
        new_run.text = text
        hyperlink.append(new_run)

        paragraph._p.append(hyperlink)

        return hyperlink

    @staticmethod
    # set table row color
    def setRowColor(row, color):
        for cell in row.cells:
            tblCell = cell._tc
            tblCellProperties = tblCell.get_or_add_tcPr()
            try:
                shdProp = tblCellProperties.xpath('./w:shd')[0]
                shdProp.set(docx.oxml.shared.qn('w:fill'), color)
            except IndexError:
                tblCellProperties.append(parse_xml(r'<w:shd {} w:fill="{}"/>'.format(nsdecls('w'), color)))
        return row


if __name__ == '__main__':
    main(sys.argv[1:])
