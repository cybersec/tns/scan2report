# MANUAL STEP: For first run check the instructions from README.

.venv\Scripts\activate.bat

# Try docx
python scan2report.py -o test.docx ../sample-lynis-report.dat ../sample-burp.xml ../sample-nessus.nessus

# Try json
python scan2report.py -o test.json ../sample-lynis-report.dat ../sample-burp.xml ../sample-nessus.nessus

# Try csv
python scan2report.py -o test.csv ../sample-lynis-report.dat ../sample-burp.xml ../sample-nessus.nessus
