FROM python:3.8

WORKDIR /app

RUN apt-get clean && apt-get update && apt-get install -y locales locales-all
RUN locale-gen cs_CZ && locale-gen cs_CZ.UTF-8 && update-locale cs_CZ.UTF-8
ENV LANG="cs_CZ.UTF-8" LC_ALL="cs_CZ.UTF-8" LC_CTYPE="cs_CZ.UTF-8"

COPY requirements.txt requirements.txt
RUN pip3 install -r requirements.txt

# print help page
ENTRYPOINT [ "python3", "scan2report.py"]
