# Scan2Report

Processing files from vulnerability scanners (Nessus, Burp, Kubeaudit, Lynis, CHAPS) to Docx, JSON, or CSV. Uses multilangual templates enhances the findings (grouping, aliasing, more info, ...).

## Setup

### Docker

```sh
# Build once
docker build --tag scan2report-local .

# Run as many times as you need.
docker run -it --rm --name scan2report -v "$PWD":/app scan2report-local -o example-output.docx <INPUT_FILES>
```

You can try it for example with the following [Nessus file](https://gitlab.fi.muni.cz/cybersec/tns/pwndocimportautomator/-/blob/main/tests/test_files/scanner_results/DefectDojo/nessus/nessus_with_cvssv3.nessus).


### Linux without Docker 

Install

```bash
sudo locale-gen cs_CZ
sudo locale-gen cs_CZ.UTF-8
sudo update-locale

# Todo: Create virtual environment to isolate the packages.
python3 -m pip uninstall docx
python3 -m pip install -U -r requirements.txt
```

Run

```bash
python3 scan2report.py -o example-output.docx <INPUT_FILES>
```

## Interface
### CLI

```
Usage: scan2report.py [OPTIONS] .. <FILE> ..
    <FILE>: Input files. Supported formats: Nessus XML report, Burp, Lynis Report (.dat), KubeAudit, CHAPS.

    OPTIONS:
        -d, --debug (enables also plugin IDs in output document)
        -i, --ignore <plugin_ids>: comma separated list of plugin ids to be ignored (ex. nessus_57582), default: none
        -I, --include <plugin_ids>: include only specified plugins (ex. nessus_57582), default: none
        -l, --lang <language>: report language (cs, en), default: cs
        -m, --min-severity <num>: minimal severity (0 - info ... 4 - critical), default: 0
        -o, --output <file>: write output to file, default: out.docx , supported formats: ['csv', 'json', 'docx']
        -p, --profile <name>: use selected profile (default, internal), defaut value: default
        -r, --resolve-redirects: resolve redirects via nessus.com, default: False
        -s, --skip-services: do not generate open ports table, default: False
        -S, --plugin-stats: do not generate report, only plugin hit stats, default: False
        -t, --template <file>: use specified template document, default: /app/templates/tns.docx
        -W, --skip-tcp-wrapped: ignore open ports detected as TCP wrapped, default: False
```

### Python

If you prefer to interact using Python, call the `main` function. You can pass both list of string arguments as the CLI would (`argv == sys.argv[1:]`) or pass a structure `Scan2ReportConfig`. If you pass both, `argv` will rewrite `Scan2ReportConfig`. The returned value is of type `Scan2ReportData`.

```py
s2r_config = scan2report.Scan2ReportConfig(lang='en', outfile='example-output.docx')

# example nessus file is not part of this repository
s2r_result: Scan2ReportData = scan2report.main(["example.nessus"], s2r_c=s2r_config) 
```

## Configuration files

#### `plugins/groups.json`

Some findings are very similar - so similar in fact that they should be one. This file defines which should be grouped.

_Key `#name` is not used, it's only for visual reference._

#### `plugins/profiles.json`

Some findings should be ignored for some pentests, as well as some sections should not be visible. For example pentest using profile `internal` will not hide some findings and not generate table of opened ports.

#### `plugins/aliases.json`

Some findings don't have to be merged, but can be aliased together and outputed only once. This file defines that.

#### `plugins/<language>​​​​​​​​​​​​​​​​​/*.json`

These folders contain vulnerability templates which are ready for use.

#### `plugins/<language>/templates/*.json`

These folders contain vulnerability templates which were automatically generated based on vulnerability scans. These findings are NOT outputed into the docx report, unless you copy them from `plugins/<language>/templates/*.json` to ``plugins/<language>​​​​​​​​​​​​​​​​​/*.json` and run scan2report again.

#### `templates/`

Place your docx templates here. It also contains configuration for Lynis report.
