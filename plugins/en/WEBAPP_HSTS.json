{
    "attack_complexicity": 3,
    "author": "Petr Svoboda",
    "description": "The webserver is using HTTPS protocol (a SSL/TLS encryption) to protect network communication. However, it does not set HTTP Strict Transport Security (HSTS) to instruct web browsers to access the server only using HTTPS (and never unencrypted HTTP).",
    "ignore_pluginoutput": "true",
    "impact": "The webserver is not using all available means to protect the communication between server and client.\n\nAn attacker, that is able to modify a user's network traffic (for example when client is connected to a public WiFi), can rewrite all links from HTTPS to HTTP and thus redirect all communication to unencrypted channel. The attacker can then obtain sensitive information or manipulate user to perform arbitrary action on affected server. **Note:** __This attack is not prevented by the fact that the server does not allow unencrypted connections over HTTP or redirects to all requests from HTTP to HTTPS. An attacker can, for example by forging DNS records of the server, redirect the communication intended for legitimate server to a server under his controll and still be able to eavesdrop or modify the communication. A HSTS header received from the server by client prior to the attack attempt, prevents the attack__.",
    "links": [
        "https://capec.mitre.org/data/definitions/157.html",
        "https://capec.mitre.org/data/definitions/94.html",
        "https://cwe.mitre.org/data/definitions/523.html",
        "https://hstspreload.org/",
        "https://https.cio.gov/hsts/",
        "https://cheatsheetseries.owasp.org/cheatsheets/HTTP_Strict_Transport_Security_Cheat_Sheet.html"
    ],
    "name": "HTTP – Strict Transport Security (HSTS) Not Enforced",
    "recommendations": "Enable HTTP Strict Transport Security (HSTS) by adding a response header with the name 'Strict-Transport-Security'. Set the value 'max-age=expireTime', where 'expireTime' is the time in seconds that browsers should remember that the site should only be accessed using HTTPS. Set the 'max-age' to at least one year (31536000s). When implementing HSTS in a production environment, it is safer to start with a small 'max-age' (e.g. 5 minutes) and then slowly increase it up to the desired value.\n\nIf the server has subdomains, add the 'includeSubDomains' flag to force encrypted communitions also to server subdomains.\n\nNote that because HSTS is a '__trust on first use__' protocol, a user who has never accessed the application will never have seen the HSTS header, and will therefore still be vulnerable to these SSL stripping attacks. To mitigate this risk, you can optionally add the 'preload' flag to the HSTS header if the server is publicly accessible, and submit the domain for review by browser vendors (see additional links).",
    "severity": 1,
    "system_impact": 2
}